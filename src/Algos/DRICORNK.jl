"""
    dricornk(
      adj_close::Matrix{T},
      adj_close_market::Vector{T},
      horizon::M,
      k::M,
      w::M,
      p::M;
      lambda::T=1e-3,
      init_budg=1,
      progress::Bool=false
    ) where {T<:Float64, M<:Int}

Run the DRICORNK algorithm.

# Arguments
- `adj_close::Matrix{T}`: A matrix of adjusted close prices of the assets.
- `adj_close_market::Vector{T}`: A vector of adjusted close prices of the market in the same period.
- `horizon::M`: The investment horizon.
- `k::M`: The number of experts.
- `w::M`: maximum length of time window to be examined.
- `p::M`: maximum number of correlation coefficient thresholds.

## Keyword Arguments
- `lambda::T=1e-3`: The regularization parameter.
- `init_budg=1`: The initial budget for investment.
- `progress::Bool=false`: Whether to show the progress bar.

!!! warning "Beware!"
    `adj_close` should be a matrix of size `n_assets` × `n_periods`.

# Returns
- `::OPSAlgorithm(n_assets, b, alg)`: An object of type `OPSAlgorithm`.

# Example
```julia
julia> using OnlinePortfolioSelection

julia> stocks_adj, market_adj = rand(10, 100), rand(100);

julia> m_dricornk = dricornk(stocks_adj, market_adj, 5, 2, 4, 3);

julia> sum(m_dricornk.b, dims=1) .|> isapprox(1.) |> all
true
```

See [`cornk`](@ref), and [`cornu`](@ref).

# Reference
> [DRICORN-K: A Dynamic RIsk CORrelation-driven Non-parametric Algorithm for Online Portfolio Selection](https://www.doi.org/10.1007/978-3-030-66151-9_12)
"""
function dricornk(
  adj_close::Matrix{T},
  adj_close_market::Vector{T},
  horizon::M,
  k::M,
  w::M,
  p::M;
  lambda::T=1e-3,
  init_budg=1,
  progress::Bool=false
) where {T<:Float64, M<:Int}

  p<2 && ArgumentError("The value of `p` should be more than 1.") |> throw
  n_experts = w*(p+1)
  k>n_experts && ArgumentError(
    "The value of k ($k) is more than number of experts ($n_experts)"
  ) |> throw
  size(adj_close, 2)-horizon>21 || ArgumentError(
    "DRICORN-K needs adequate number of data samples to determine the \
    market condition. With considering the passed arguments, you have to at least add \
    $(21-size(adj_close, 2)+horizon+1) more samples."
  ) |> throw

  # Calculate relative prices
  relative_prices = adj_close[:, 2:end] ./ adj_close[:, 1:end-1]
  # Market's return
  market_ret = log.(adj_close_market[2:end] ./ adj_close_market[1:end-1])
  # Stocks' return
  asset_ret  = log.(relative_prices)
  n_assets   = size(relative_prices, 1)
  P          = (iszero(pᵢ) ? 0. : (pᵢ-1)/pᵢ for pᵢ=0:p)
  q          = 1/k
  weights    = zeros(T, n_assets, horizon)
  Sₜ_        = zeros(T, n_experts, horizon+1)
  Sₜ_[:, 1] .= init_budg
  for t ∈ 0:horizon-1
    bₜ = Matrix{T}(undef, n_assets, n_experts)
    expert = 1
    for ω ∈ 1:w
      for ρ ∈ P
        b = dricorn_expert(
          relative_prices,
          asset_ret,
          market_ret,
          horizon,
          ω,
          ρ,
          t,
          n_assets,
          lambda
        )

        bₜ[:, expert]    = b
        Sₜ_[expert, t+2] = S(
          Sₜ_[expert, t+1], b, relative_prices[:, end-horizon+t+1]
        )

        expert += 1
      end
    end

    idx_top_k       = sortperm(Sₜ_[:, t+2], rev=true)[1:k]
    weights[:, t+1] = final_weights(q, Sₜ_[idx_top_k, t+2], bₜ[:, idx_top_k])
    progress && progressbar(stdout, horizon, t+1)
  end

  return OPSAlgorithm(n_assets, weights, "DRICORN-K")
end

"""
    dricorn_expert(
      relative_prices::Matrix{T},
      asset_ret::Matrix{T},
      market_ret::Vector{T},
      horizon::S,
      w::S,
      rho::T,
      t::S,
      n_assets::S,
      lambda::T
    ) where {T<:Float64, S<:Int}

Create an expert to perform the algorithm according to the given parameters.

# Arguments
- `relative_prices::Matrix{T}`: A matrix of relative prices of the assets.
- `asset_ret::Matrix{T}`: A matrix of assets' returns.
- `market_ret::Vector{T}`: A vector of market's returns.
- `horizon::S`: The investment horizon.
- `w::S`: length of time window to be examined.
- `rho::T`: The correlation coefficient threshold.
- `t::S`: The current period index.
- `n_assets::S`: The number of assets.
- `lambda::T`: The regularization parameter.

# Returns
- `weight::Vector{T}`: The weights of the assets for the period `t`.
"""
function dricorn_expert(
  relative_prices::Matrix{T},
  asset_ret::Matrix{T},
  market_ret::Vector{T},
  horizon::S,
  w::S,
  rho::T,
  t::S,
  n_assets::S,
  lambda::T
) where {T<:Float64, S<:Int}

  horizon≥size(relative_prices, 2) && ArgumentError("""The "horizon" ($horizon) is \
    bigger than data samples $(size(relative_prices, 2)).\nYou should either decrease \
    the "horizon" or add more data samples. (At least $(horizon-size(relative_prices, 2)) \
    more data samples are needed)."""
  ) |> throw

  shift            = horizon+t
  ρ                = rho
  relative_prices_ = relative_prices[:, 1:end-shift]
  n_periods        = size(relative_prices_, 2)
  # Coefficient behind βₚ. Check if the market return is at least 20% higher
  # than the return of 20 days before the current time window. If it is, then
  # the coefficient is 1. Otherwise, it is -1.
  market_ret[end-shift]/market_ret[end-shift-20] ≥ 1.2 ? c = 1 : c = -1
  # index of similar time windows
  idx_tws = locate_sim(relative_prices_, w, n_periods, ρ)
  isempty(idx_tws) && return fill(1/n_assets, n_assets)
  # index of a day after similar time windows
  idx_days = idx_tws.+w
  # Calculate β of each asset through the last month (20 days)
  β = zeros(T, n_assets, length(idx_days))
  for i ∈ 1:n_assets
    β[i, :] .= cor(
      asset_ret[i, end-shift-20:end-shift],
      market_ret[end-shift-20:end-shift]
    )/var(market_ret[end-shift-20:end-shift])
  end

  model = Model(Optimizer)
  set_silent(model)

  @variable(model, 0<=b[i=1:n_assets]<=1)
  @constraint(model, sum(b) == 1)
  @expression(model, h, (b' * relative_prices_[:, idx_days] .+ lambda*c.*(b'*β)))
  @NLobjective(model, Max, prod(h[i] for i=eachindex(h)))
  optimize!(model)
  weight = value.(b)
  weight = round.(abs.(weight), digits=3)
  isapprox(1., sum(weight), atol=1e-2) || normalizer!(weight)

  return weight
end
